DROP DATABASE IF EXISTS socialdb;

CREATE DATABASE socialdb;

USE socialdb;

CREATE TABLE users (
       id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
       lastname VARCHAR(255) NOT NULL,
       firstname VARCHAR(255) NOT NULL,
       mail VARCHAR(255) NOT NULL,
       password VARCHAR(255) NOT NULL
);

CREATE TABLE relationships (
       me INT NOT NULL,
       other INT NOT NULL,
       FOREIGN KEY(me) REFERENCES users(id),
       FOREIGN KEY(other) REFERENCES users(id),
       PRIMARY KEY (me, other)
);

CREATE TABLE medias (
       id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
       `path` VARCHAR(255) NOT NULL,
       `type` INT NOT NULL
);

CREATE TABLE posts (
       id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
       message TEXT NOT NULL,
       author INT NOT NULL,
       `date` DATETIME NOT NULL,
       media INT,
       FOREIGN KEY(author) REFERENCES users(id),
       FOREIGN KEY(media) REFERENCES medias(id)
);

CREATE TABLE likes (
       post INT NOT NULL,
       `user` INT NOT NULL,
       FOREIGN KEY(post) REFERENCES posts(id),
       FOREIGN KEY(`user`) REFERENCES users(id),
       PRIMARY KEY (post, user)
);

CREATE TABLE comments (
       id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
       post INT NOT NULL,
       message TEXT NOT NULL,
       author INT NOT NULL,
       `date` DATETIME NOT NULL,
       FOREIGN KEY(author) REFERENCES users(id),
       FOREIGN KEY(post) REFERENCES posts(id)
);

INSERT INTO `users`(id, lastname, firstname, mail, password) VALUES
  (1, "Last", "First", "demo@demo.fr", "demo"),
  (2, "Nicolas", "Faure", "nicolas.faure.7@gmail.com", "demo");

INSERT INTO `posts`(id, message, author, date, media) VALUES
  (1, "Demo first post", 1, "2015-07-11 17:40:32", NULL),
  (2, "Demo second post", 1, "2015-07-11 17:55:21", NULL);
