Vaadin+
=======

Vaadin+ is a social network written in Java with the Vaadin framework to manage the UI.

Workflow
========

To compile the entire project, run "mvn install".
To run the application, run "mvn jetty:run" and open http://localhost:8080/ .