package fr.yaka.ui;

import com.vaadin.cdi.CDIView;
import com.vaadin.data.Validator;
import com.vaadin.data.validator.EmailValidator;
import com.vaadin.data.validator.StringLengthValidator;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.shared.ui.MarginInfo;
import com.vaadin.ui.*;
import com.vaadin.ui.themes.Reindeer;
import fr.yaka.entities.UsersEntity;
import fr.yaka.ui.custom.MenuBarCustom;

import javax.inject.Inject;
import javax.inject.Named;


@CDIView("register")
public class RegisterView extends CustomComponent implements View, Button.ClickListener {
    public static final String NAME = "register";

    private final TextField lastname, firstname, email;
    private final PasswordField password, checkPassword;
    private final Button okButton;
	private final MenuBarCustom mbc;

	@Inject
	@Named("Users")
	private fr.yaka.dao.Users usersDao;

    public RegisterView()
    {
	    mbc = new MenuBarCustom();

        // Validators
        final StringLengthValidator lastnameLengthValidator, firstnameLengthValidator, passwordLengthValidator;
        final EmailValidator emailValidator;
        lastnameLengthValidator = new StringLengthValidator("Your lastname must contain at least 2 letters and, at most 50", 2, 50, false);
        firstnameLengthValidator = new StringLengthValidator("Your firstname must contain at least 2 letters and, at most 50", 2, 50, false);
        emailValidator = new EmailValidator("Your mail address is incorrect");
        passwordLengthValidator = new StringLengthValidator("Your password is too short", 4, 50, false);


        // Fields
        lastname = new TextField("Lastname");
        lastname.setRequired(true);
        lastname.addValidator(lastnameLengthValidator);
        firstname = new TextField("Firstname");
        firstname.setRequired(true);
        firstname.addValidator(firstnameLengthValidator);
        email = new TextField("Mail");
        email.setRequired(true);
        email.addValidator(emailValidator);
        password = new PasswordField("Password");
        password.setRequired(true);
        password.addValidator(passwordLengthValidator);
        checkPassword = new PasswordField("Check password");
        checkPassword.setRequired(true);
        checkPassword.addValidator(new PasswordConfirmValidator("Passwords don't match", password));
        okButton = new Button("Validate");
        okButton.addClickListener(this);

        // Layouts
        setSizeFull();
        final VerticalLayout mainVerticalLayout = new VerticalLayout();
        final VerticalLayout fields = new VerticalLayout(lastname, firstname, email, password, checkPassword, okButton);
        fields.setSpacing(true);
        fields.setMargin(new MarginInfo(true, true, true, false));
        fields.setSizeUndefined();

        mainVerticalLayout.setStyleName(Reindeer.LAYOUT_BLUE);
        mainVerticalLayout.addComponent(mbc);
        final HorizontalLayout horizontalFieldsLayout = new HorizontalLayout(fields);
        horizontalFieldsLayout.setSpacing(true);
        horizontalFieldsLayout.setSizeFull();
        horizontalFieldsLayout.setComponentAlignment(fields, Alignment.MIDDLE_CENTER);
        mainVerticalLayout.addComponent(horizontalFieldsLayout);

        setCompositionRoot(mainVerticalLayout);
    }
    @Override
    public void enter(ViewChangeListener.ViewChangeEvent viewChangeEvent)
    {
	    mbc.init();
    }

    @Override
    public void buttonClick(Button.ClickEvent clickEvent) {
        try {

            lastname.validate();
            firstname.validate();
            email.validate();
            password.validate();
            checkPassword.validate();

            if (usersDao.get(email.getValue()) != null) {
                throw new Validator.InvalidValueException("User " + email.getValue() + " already exist");
            }

            UsersEntity user = new UsersEntity();
            user.setMail(email.getValue());
            user.setLastname(lastname.getValue());
            user.setFirstname(firstname.getValue());
            user.setPassword(password.getValue());
            usersDao.save(user);
            getUI().getNavigator().navigateTo(LoginView.NAME);

        } catch (Validator.InvalidValueException ex) {
            final String exceptionString = ex.getMessage();
            if (exceptionString.equals(""))
                 Notification.show("Field(s) can't be empty");
            else
                Notification.show(exceptionString);
        }
        catch (Exception ex) {
            Notification.show("Internal error, sorry ... :(");
        }
    }

    private class PasswordConfirmValidator implements Validator {

        final private String message;
        final private PasswordField passwordField;
        public PasswordConfirmValidator(String msg, PasswordField passwordField) {
            message = msg;
            this.passwordField = passwordField;
        }

        @Override
        public void validate(Object o) throws InvalidValueException {
            if (!o.equals(this.passwordField.getValue()))
                throw new Validator.InvalidValueException(message);
        }
    }
}
