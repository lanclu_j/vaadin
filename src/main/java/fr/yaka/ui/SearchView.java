package fr.yaka.ui;

import com.vaadin.cdi.CDIView;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.shared.ui.MarginInfo;
import com.vaadin.ui.*;
import com.vaadin.ui.themes.Reindeer;
import fr.yaka.entities.UsersEntity;
import fr.yaka.listener.RelationChangeClickListener;
import fr.yaka.ui.custom.ListUserCustom;
import fr.yaka.ui.custom.MenuBarCustom;

import javax.inject.Inject;
import javax.inject.Named;
import java.util.List;

@CDIView("search")
public class SearchView extends CustomComponent implements View, Button.ClickListener
{
    public static final String NAME = "search";
    private final TextField searchData;
    private final Button searchValidate;
    private ListUserCustom results;
	private final MenuBarCustom mbc;
    private final VerticalLayout viewLayout;

	@Inject
	@Named("Users")
	private fr.yaka.dao.Users usersDao;

    @Inject
    @Named("Relationships")
    private fr.yaka.dao.Relationships relationshipsDao;

    public SearchView() {
        searchData = new TextField();
        searchValidate = new Button("Search", this);
        mbc = new MenuBarCustom();

        HorizontalLayout searchPanel = new HorizontalLayout(searchData, searchValidate);
        searchPanel.setSpacing(true);
        searchPanel.setMargin(new MarginInfo(true, true, true, false));
        searchPanel.setSizeUndefined();

        // The view root layout
        viewLayout = new VerticalLayout(mbc, searchPanel);
        viewLayout.setSizeFull();
        viewLayout.setComponentAlignment(searchPanel, Alignment.MIDDLE_CENTER);

        setCompositionRoot(viewLayout);
    }

    @Override
    public void enter(ViewChangeListener.ViewChangeEvent viewChangeEvent)
    {
        // Add to a panel
        results = new ListUserCustom(new RelationChangeClickListener(usersDao, relationshipsDao));
        results.setSpacing(true);
        results.setMargin(new MarginInfo(true, true, true, false));
        results.setSizeUndefined();

        viewLayout.addComponent(results);

        viewLayout.setComponentAlignment(results, Alignment.MIDDLE_CENTER);
        viewLayout.setStyleName(Reindeer.LAYOUT_BLUE);

        mbc.init();
    }

    @Override
    public void buttonClick(Button.ClickEvent clickEvent) {
        results.removeAllComponents();
        List<UsersEntity> users = usersDao.search(searchData.getValue());

        results.addUsers(users);

        if (users.size() == 0)
            Notification.show("Can't find any person with that name");
    }
}
