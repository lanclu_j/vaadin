package fr.yaka.ui.custom;

import com.vaadin.server.Sizeable;
import com.vaadin.ui.MenuBar;
import com.vaadin.ui.UI;
import fr.yaka.entities.UsersEntity;
import fr.yaka.ui.*;

public class MenuBarCustom extends MenuBar
{
    public MenuBarCustom()
    {
        this.setWidth(100.0f, Sizeable.Unit.PERCENTAGE);
        this.setHeight(35f, Unit.PIXELS);
    }

    public void init()
    {
        this.removeItems();
        MenuBar.MenuItem title = this.addItem("Vaadin+", i -> UI.getCurrent().getNavigator().navigateTo(DashboardView.NAME));

		if (getSession() != null && getSession().getAttribute("user") != null)
		{
			UsersEntity me = (UsersEntity)getSession().getAttribute("user");

			MenuBar.MenuItem user = this.addItem(me.getFirstname() + " " + me.getLastname(), null);
            user.setStyleName("menuRight");
			MenuBar.MenuItem profile = user.addItem("My profile", i -> UI.getCurrent().getNavigator().navigateTo(ProfileView.NAME + "/" + me.getId()));

            user.addSeparator();
            MenuBar.MenuItem subscriptions = user.addItem("My subscriptions", i -> UI.getCurrent().getNavigator().navigateTo(SubscriptionView.NAME));
            //MenuBar.MenuItem followers = user.addItem("My followers", i -> UI.getCurrent().getNavigator().navigateTo(FollowerView.NAME));

            user.addSeparator();
            MenuBar.MenuItem signout = user.addItem("Sign out", i -> {
                getSession().setAttribute("user", null);
                getUI().getNavigator().navigateTo(LoginView.NAME);
            });

            MenuBar.MenuItem search = this.addItem("Search", i -> UI.getCurrent().getNavigator().navigateTo(SearchView.NAME));
            search.setStyleName("menuRight");
        }
    }
}
