package fr.yaka.ui.custom;

import com.vaadin.ui.*;
import fr.yaka.dao.Comments;
import fr.yaka.dao.Users;
import fr.yaka.entities.CommentsEntity;
import fr.yaka.entities.UsersEntity;
import fr.yaka.ui.DashboardView;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

public class CommentLayout extends VerticalLayout {

    private final Users usersDao;
    private final Comments commentsDao;
    private UsersEntity currentUser;
    private int postId;

    private VerticalLayout commentFormLayout, commentsListLayout;

    public CommentLayout(Users usersDao, Comments commentsDao)
    {
        this.usersDao = usersDao;
        this.commentsDao = commentsDao;
        this.setSpacing(true);
        this.setMargin(true);
    }

    public void displayComments(UsersEntity currentUser, int postId)
    {
        this.currentUser = currentUser;
        this.postId = postId;
        commentsListLayout = new VerticalLayout();
        commentsListLayout.setSpacing(true);
        commentsListLayout.setMargin(true);
        initCommentsForm();
        initCommentsList();
    }

    private void initCommentsForm()
    {
        TextArea commentArea = new TextArea();
        commentArea.setInputPrompt("Enter your comment");
        commentArea.setImmediate(true);
        commentArea.setSizeFull();

        Button publishButton = new Button("Publish", clickEvent -> {
           String msg = commentArea.getValue();

            if (!msg.isEmpty()) {
                try {
                    // Post new status
                    CommentsEntity newComment = new CommentsEntity();
                    newComment.setAuthor(currentUser.getId());
                    newComment.setDate(new Timestamp(System.currentTimeMillis()));
                    newComment.setMessage(msg);
                    newComment.setPost(postId);
                    newComment = commentsDao.save(newComment);

                    addComment(newComment);
                    commentArea.setValue("");
                    getUI().getNavigator().navigateTo(DashboardView.NAME);
                } catch (Exception e) {
                    Notification.show("Error when posting new comment.");
                }
            }
        });

        commentFormLayout = new VerticalLayout(commentArea, publishButton);
        commentFormLayout.setSpacing(true);
        commentFormLayout.setMargin(true);
        this.addComponent(commentFormLayout);
    }

    private void initCommentsList()
    {
        List<CommentsEntity> comments = commentsDao.findAll()
                                                    .stream().filter(x -> x.getPost() == postId)
                                                    .collect(Collectors.toList());
        comments.forEach(x -> addComment(x));
        this.addComponent(commentsListLayout);
    }

    private void addComment(CommentsEntity comment)
    {
        UsersEntity author = usersDao.find(comment.getAuthor());

        String title = String.format("%s %s - %s",
                author.getFirstname(),
                author.getLastname(),
                new Date(comment.getDate().getTime()).toString());

        Panel panel = new Panel(title);
        panel.setContent(new Label(comment.getMessage()));

        commentsListLayout.addComponent(panel, 0);
    }
}
