package fr.yaka.ui.custom;

import com.vaadin.ui.Button;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.VerticalLayout;
import fr.yaka.entities.RelationshipsEntity;
import fr.yaka.entities.UsersEntity;

import java.util.Collection;

public class ListUserCustom extends VerticalLayout {

    private Button.ClickListener listener;

    public ListUserCustom ( Button.ClickListener listener ) {
        this.listener = listener;
    }

    public ListUserCustom (Collection<UsersEntity> users, Button.ClickListener listener) {
        this.addUsers(users);
        this.listener = listener;
    }

    public void addUsers(Collection<UsersEntity> users) {
        UsersEntity current = (UsersEntity)getSession().getAttribute("user");
        for (UsersEntity user : users) {
            GridLayout grid = new GridLayout();
            grid.addComponent(new Label(user.getFirstname() + " " + user.getLastname()));
            grid.addComponent(new Label(user.getMail()));
            grid.addComponent(new Button("View profile", event -> {
                    getUI().getNavigator().navigateTo("profile/" + user.getId());
            }));
            Button relation = new Button("Unfollow");
            relation.setData(user);
            RelationshipsEntity rel = new RelationshipsEntity();
            rel.setMe(current.getId());
            rel.setOther(user.getId());
            rel.setUsersByMe(current);
            rel.setUsersByOther(user);
            if (!current.getRelationshipsesById().contains(rel)) {
                relation.setCaption("Follow");
            }
            relation.addClickListener(listener);
            if (current.getId() != user.getId())
                grid.addComponent(relation);

            this.addComponent(grid);
        }
    }

}
