package fr.yaka.ui;

import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.shared.ui.MarginInfo;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.CustomComponent;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.Reindeer;
import fr.yaka.entities.UsersEntity;
import fr.yaka.listener.RelationChangeClickListener;
import fr.yaka.ui.custom.ListUserCustom;
import fr.yaka.ui.custom.MenuBarCustom;

import javax.inject.Inject;
import javax.inject.Named;
import java.util.Collection;
import java.util.stream.Collectors;

/**
 * Created by Jimmy on 11/07/15.
 */
public class FollowerView extends CustomComponent implements View {

    public static final String NAME = "subscription";

    private final MenuBarCustom mbc;
    private ListUserCustom results;
    private final VerticalLayout viewLayout;

    @Inject
    @Named("Users")
    private fr.yaka.dao.Users usersDao;

    @Inject
    @Named("Relationships")
    private fr.yaka.dao.Relationships relationshipsDao;

    public FollowerView() {
        mbc = new MenuBarCustom();

        // The view root layout
        viewLayout = new VerticalLayout();
        viewLayout.addComponent(mbc);

        viewLayout.setSizeFull();
        viewLayout.setCaption("My followers list");

        viewLayout.setStyleName(Reindeer.LAYOUT_BLUE);

        setCompositionRoot(viewLayout);
    }

    @Override
    public void enter(ViewChangeListener.ViewChangeEvent viewChangeEvent) {
        // Add to a panel
        if (results != null)
            results.removeAllComponents();
        else {
            results = new ListUserCustom(new RelationChangeClickListener(usersDao, relationshipsDao));
            results.setSpacing(true);
            results.setMargin(new MarginInfo(true, true, true, false));
            results.setSizeUndefined();

            viewLayout.addComponent(results);
            viewLayout.setComponentAlignment(results, Alignment.TOP_CENTER);
            mbc.init();
        }

        UsersEntity me = (UsersEntity)getSession().getAttribute("user");

        Collection<UsersEntity> userFriends = me.getRelationshipsesById_0().stream().map(relation -> usersDao.find(relation.getOther())).collect(Collectors.toList());
        results.addUsers(userFriends);
    }
}
