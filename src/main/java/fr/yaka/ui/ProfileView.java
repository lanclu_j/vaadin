package fr.yaka.ui;

import com.google.gwt.safehtml.shared.SafeHtmlUtils;
import com.vaadin.cdi.CDIView;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.*;
import com.vaadin.ui.themes.Reindeer;
import fr.yaka.entities.LikesEntity;
import fr.yaka.entities.PostsEntity;
import fr.yaka.entities.RelationshipsEntity;
import fr.yaka.entities.UsersEntity;
import fr.yaka.listener.RelationChangeClickListener;
import fr.yaka.ui.custom.CommentLayout;
import fr.yaka.ui.custom.MenuBarCustom;

import javax.inject.Inject;
import javax.inject.Named;
import java.sql.Date;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

@CDIView("profile")
public class ProfileView extends CustomComponent implements View
{
    public static final String NAME = "profile";
	private final VerticalLayout viewLayout;
	private final VerticalLayout timeline;
    private final MenuBarCustom mbc;

	private Button followButton;
	private UsersEntity user;
	private boolean isFollowing;
	private TextField searchData;

	@Inject
	@Named("Users")
	private fr.yaka.dao.Users usersDao;

	@Inject
	@Named("Relationships")
	private fr.yaka.dao.Relationships relationshipsDao;

	@Inject
	@Named("Likes")
	private fr.yaka.dao.Likes likesDao;

	@Inject
	@Named("Comments")
	private fr.yaka.dao.Comments commentsDao;

    public ProfileView() {
	    mbc = new MenuBarCustom();

	    // The view root layout
	    viewLayout = new VerticalLayout();
	    viewLayout.setSizeFull();
	    viewLayout.setStyleName(Reindeer.LAYOUT_BLUE);

	    timeline = new VerticalLayout();
	    timeline.setSizeFull();

	    setCompositionRoot(viewLayout);
    }

    @Override
    public void enter(ViewChangeListener.ViewChangeEvent viewChangeEvent)
    {
	    viewLayout.removeAllComponents();
	    timeline.removeAllComponents();

	    viewLayout.addComponent(mbc);
	    mbc.init();

	    int user_id = Integer.parseInt(viewChangeEvent.getParameters());
	    user = usersDao.find(user_id);

	    UsersEntity me = (UsersEntity)getSession().getAttribute("user");
	    isFollowing = false;
	    for (RelationshipsEntity rel : me.getRelationshipsesById())
	    {
		    if (rel.getMe() == me.getId() &&  rel.getOther() == user.getId())
		    {
			    isFollowing = true;
			    break;
		    }
	    }

        Label l = new Label("<h2>" + SafeHtmlUtils.htmlEscape(user.getFirstname() + " " + user.getLastname()) + "</h2>", ContentMode.HTML);
	    VerticalLayout vl = new VerticalLayout(l);

	    if (user_id != me.getId())
	    {
		    followButton = new Button(isFollowing ? "Unfollow" : "Follow", new RelationChangeClickListener(usersDao, relationshipsDao));
            vl.addComponent(followButton);
        }

        HorizontalLayout hl = new HorizontalLayout(vl);
        hl.setComponentAlignment(vl, Alignment.MIDDLE_CENTER);
        viewLayout.addComponent(hl);
        viewLayout.setSpacing(true);
        viewLayout.setComponentAlignment(hl, Alignment.TOP_CENTER);

	    Collection<PostsEntity> posts = user.getPostsesById();
	    Comparator cmp = new Comparator<PostsEntity>() {
		    public int compare(PostsEntity c1, PostsEntity c2) {
			    return (int)(c2.getDate().getTime() - c1.getDate().getTime());
		    }
	    };
	    Collections.sort((List) posts, cmp);

	    viewLayout.addComponent(timeline);
	    posts.forEach(this::displayStatus);
    }
	private void displayStatus(PostsEntity post) {
		UsersEntity me = (UsersEntity)getSession().getAttribute("user");
		UsersEntity author = post.getUsersByAuthor();

		int likes = post.getLikesesById().size();
		String title = String.format("%s %s - %s - %d like%s",
				author.getFirstname(),
				author.getLastname(),
				new java.util.Date(post.getDate().getTime()).toString(),
				likes,
				likes > 1 ? "s" : "");
		Panel panel = new Panel(title);

		boolean isLiked = me.likesPost(post) != null;
		Button likeButton = new Button(isLiked ? "Unlike" : "Like", clickEvent -> {
			LikesEntity liked = me.likesPost(post);

			clickEvent.getButton().setEnabled(false);
			if (liked != null)
			{
				likesDao.delete(liked);
				me.getLikesesById().remove(liked);
				post.getLikesesById().remove(liked);

				getSession().setAttribute("user", me);
			}
			else
			{
				LikesEntity rel = new LikesEntity();
				rel.setUser(me.getId());
				rel.setPost(post.getId());

				me.getLikesesById().add(rel);
				post.getLikesesById().add(rel);
				likesDao.save(rel);

				getSession().setAttribute("user", me);
			}

			clickEvent.getButton().setCaption(liked == null ? "Unlike" : "Like");
			clickEvent.getButton().setEnabled(true);

			String newtitle = String.format("%s %s - %s - %d like%s",
					author.getFirstname(),
					author.getLastname(),
					new java.util.Date(post.getDate().getTime()).toString(),
					post.getLikesesById().size(),
					post.getLikesesById().size() > 1 ? "s" : "");
			panel.setCaption(newtitle);
		});

		Button commentButton = new Button("Comment", clickEvent -> {
			Window modal = new Window("Comments");
			modal.setWidth(500.0f, Unit.PIXELS);

			CommentLayout comments = new CommentLayout(usersDao, commentsDao);
			comments.displayComments(user, post.getId());
			modal.setContent(comments);
			modal.setModal(true);
			getUI().getCurrent().addWindow(modal);
		});

		Label date = new Label();

		HorizontalLayout actionLayout = new HorizontalLayout(likeButton, commentButton, date);

		VerticalLayout content = new VerticalLayout(new Label(post.getMessage()), actionLayout);
		content.setWidth(100f, Unit.PERCENTAGE);
		content.setMargin(true);

		panel.setContent(content);
		timeline.addComponent(panel);
	}
}
