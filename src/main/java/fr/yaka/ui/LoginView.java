package fr.yaka.ui;

import com.vaadin.cdi.CDIView;
import com.vaadin.data.Validator;
import com.vaadin.data.validator.EmailValidator;
import com.vaadin.data.validator.StringLengthValidator;
import com.vaadin.event.ShortcutAction;
import com.vaadin.event.ShortcutListener;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.shared.ui.MarginInfo;
import com.vaadin.ui.*;
import com.vaadin.ui.themes.Reindeer;
import fr.yaka.entities.UsersEntity;
import fr.yaka.ui.custom.MenuBarCustom;

import javax.inject.Inject;
import javax.inject.Named;

@CDIView("login")
public class LoginView extends CustomComponent implements View, Button.ClickListener
{
    public static final String NAME = "login";
    private final TextField email;
    private final PasswordField password;
    private final Button loginButton;
    private final Button registerButton;
    private final StringLengthValidator passwordValidator;
	private final MenuBarCustom mbc;

	@Inject
	@Named("Users")
	private fr.yaka.dao.Users usersDao;

    public LoginView() {
        setSizeFull();

        // Initialize validator
        passwordValidator = new StringLengthValidator("Password is invalid", 2, 50, false);

        // Initialize email textfield
        email = new TextField("Email address");
        email.setRequired(true);
        email.setInputPrompt("username@vaadin.plus");
        email.addValidator(new EmailValidator("email is invalid"));
        email.setInvalidAllowed(false);

        final Button.ClickListener clickListener = this;
        email.addShortcutListener(new ShortcutListener("Validation", ShortcutAction.KeyCode.ENTER, null) {
            @Override
            public void handleAction(Object sender, Object target) {
                clickListener.buttonClick(null);
            }
        });

        // Initialize password field
        password = new PasswordField("Password");
        password.setRequired(true);
        password.addValidator(passwordValidator);
        password.setValue("");
        password.setNullRepresentation("");

        // Initialize login button
        loginButton = new Button("Sign in", this);
        registerButton = new Button("Register", clickEvent -> {
           getUI().getNavigator().navigateTo(RegisterView.NAME);
        });

        // Add both to a panel
        VerticalLayout fields = new VerticalLayout(email, password, loginButton, registerButton);
        fields.setSpacing(true);
        fields.setMargin(new MarginInfo(true, true, true, false));
        fields.setSizeUndefined();
        fields.setComponentAlignment(loginButton, Alignment.MIDDLE_CENTER);
        fields.setComponentAlignment(registerButton, Alignment.MIDDLE_CENTER);

        // The view root layout
        mbc = new MenuBarCustom();
        VerticalLayout viewLayout = new VerticalLayout(mbc, fields);

        viewLayout.setSizeFull();
        viewLayout.setExpandRatio(fields, 1l);
        viewLayout.setComponentAlignment(fields, Alignment.TOP_CENTER);
        viewLayout.setStyleName(Reindeer.LAYOUT_BLUE);
        setCompositionRoot(viewLayout);
    }

    @Override
    public void enter(ViewChangeListener.ViewChangeEvent viewChangeEvent)
    {
	    mbc.init();
        email.focus();
    }

    @Override
    public void buttonClick(Button.ClickEvent clickEvent) {
        try {
            // Basic field validation
            email.validate();
            password.validate();

            String email = this.email.getValue();
            String password = this.password.getValue();

            // Validate username and password with database here. For examples sake
            // I use a dummy username and password.
            UsersEntity user = usersDao.get(email);

            boolean isValid = user != null
                    && password.equals(user.getPassword());

            if (isValid) {
                // Store the current user in the service session
                getSession().setAttribute("user", user);

                // Navigate to main view
                getUI().getNavigator().navigateTo(DashboardView.NAME);
            } else {
                // Wrong password clear the password field and refocuses it
                this.password.setValue(null);
                this.password.focus();
                Notification.show("Wrong password.");
            }
        } catch (Validator.InvalidValueException ex) {
            final String exceptionString = ex.getMessage();
            if (exceptionString.equals(""))
                Notification.show("Field(s) can't be empty");
            else
                Notification.show(exceptionString);
        }
    }
}
