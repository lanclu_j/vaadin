package fr.yaka.ui;

import com.vaadin.cdi.CDIView;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.server.ExternalResource;
import com.vaadin.ui.*;
import com.vaadin.ui.themes.ValoTheme;
import fr.yaka.entities.LikesEntity;
import fr.yaka.entities.PostsEntity;
import fr.yaka.entities.RelationshipsEntity;
import fr.yaka.entities.UsersEntity;
import fr.yaka.ui.custom.CommentLayout;
import fr.yaka.ui.custom.MenuBarCustom;

import javax.inject.Inject;
import javax.inject.Named;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;


@CDIView("dashboard")
public class DashboardView extends CustomComponent implements View {
    public static final String NAME = "dashboard";
    private final VerticalLayout statusForm, postsLayout, listUserCustom;
    private TextArea statusArea;
    private Button statusButton;
	private final MenuBarCustom mbc;

    @Inject
    @Named("Users")
    private fr.yaka.dao.Users usersDao;

    @Inject
    @Named("Posts")
    private fr.yaka.dao.Posts postsDao;

    @Inject
    @Named("Likes")
    private fr.yaka.dao.Likes likesDao;

    @Inject
    @Named("Comments")
    private fr.yaka.dao.Comments commentsDao;

    private UsersEntity currentUser;

    public DashboardView() {
        mbc = new MenuBarCustom();
        listUserCustom = new VerticalLayout();

        // Initialize components
        initializeStatusForm();
        statusForm = new VerticalLayout(statusArea, statusButton);
        statusForm.setWidth(95.0f, Unit.PERCENTAGE);
        statusForm.setSpacing(true);
        statusForm.setMargin(true);

        postsLayout = new VerticalLayout();
        postsLayout.setWidth(95.f, Unit.PERCENTAGE);
        postsLayout.setHeight(100, Unit.PERCENTAGE);
        postsLayout.setSpacing(true);
        postsLayout.setMargin(true);

        HorizontalLayout hl = new HorizontalLayout();
        hl.setMargin(true);
        hl.setSpacing(true);
        listUserCustom.setCaption("My subscriptions");
        listUserCustom.setWidth(20, Unit.PERCENTAGE);
        listUserCustom.setHeight(100, Unit.PERCENTAGE);
        listUserCustom.setSpacing(true);

        VerticalLayout vl = new VerticalLayout();
        vl.addComponent(statusForm);
        vl.addComponent(postsLayout);

        hl.addComponents(vl, listUserCustom);
        hl.setSizeFull();
        hl.setExpandRatio(vl, 0.7f);
        hl.setExpandRatio(listUserCustom, 0.2f);

        addStyleName(ValoTheme.PANEL_WELL);

        // Add components
        VerticalLayout mainLayout = new VerticalLayout(mbc, hl);

        mainLayout.setHeight(100.0f, Unit.PERCENTAGE);
        mainLayout.setSpacing(true);

        setCompositionRoot(mainLayout);
    }

    private void initializeStatusForm() {
        statusArea = new TextArea();
        statusArea.setInputPrompt("How're you doing today?");
        statusArea.setImmediate(true);
        statusArea.setSizeFull();
        statusButton = new Button("Publish", clickEvent -> {
            String status = statusArea.getValue();

            if (!status.isEmpty()) {
                try {
                    // Post new status
                    PostsEntity newPost = new PostsEntity();
                    newPost.setAuthor(currentUser.getId());
                    newPost.setDate(new Timestamp(System.currentTimeMillis()));
                    newPost.setMessage(status);
                    newPost.setLikesesById(new ArrayList<LikesEntity>());
                    newPost = postsDao.save(newPost);

                    addPost(newPost);
                    statusArea.setValue("");
                    getUI().getNavigator().navigateTo(DashboardView.NAME);
                } catch (Exception e) {
                    Notification.show("Error when posting new status.");
                }
            }

        });
    }

    @Override
    public void enter(ViewChangeListener.ViewChangeEvent viewChangeEvent) {
        mbc.init();

        postsLayout.removeAllComponents();
        UsersEntity me = (UsersEntity)getSession().getAttribute("user");

        listUserCustom.removeAllComponents();
        for(RelationshipsEntity relation : me.getRelationshipsesById())
        {
            UsersEntity tmpUser = usersDao.find(relation.getOther());

            HorizontalLayout userLayout = new HorizontalLayout();
            Link b = new Link(tmpUser.getFirstname() + "&nbsp;" + tmpUser.getLastname(), new ExternalResource("#!" + ProfileView.NAME + "/" + tmpUser.getId()));
            b.setCaptionAsHtml(true);
            userLayout.addComponent(b);
            listUserCustom.addComponent(userLayout);
        }
        // Fetch current user
        if (getSession() != null) {
            currentUser = (UsersEntity) getSession().getAttribute("user");
            postsDao.findAll().stream().filter(post -> post.getAuthor() == currentUser.getId() ||
                currentUser.getRelationshipsesById().stream().anyMatch(relation -> relation.getOther() == post.getAuthor())).forEach(x -> addPost(x));
        } else
            getUI().getNavigator().navigateTo(LoginView.NAME);
    }

    public void addPost(PostsEntity post) {
        UsersEntity author = usersDao.find(post.getAuthor());

        int likes = post.getLikesesById().size();
        String title = String.format("%s %s - %s - %d like%s",
                author.getFirstname(),
                author.getLastname(),
                new Date(post.getDate().getTime()).toString(),
                likes,
                likes > 1 ? "s" : "");
        Panel panel = new Panel(title);

        boolean isLiked = currentUser.likesPost(post) != null;
        Button likeButton = new Button(isLiked ? "Unlike" : "Like", clickEvent -> {
            LikesEntity liked = currentUser.likesPost(post);

            clickEvent.getButton().setEnabled(false);
            if (liked != null) {
                likesDao.delete(liked);
                currentUser.getLikesesById().remove(liked);
                post.getLikesesById().remove(liked);

                getSession().setAttribute("user", currentUser);
            } else {
                LikesEntity rel = new LikesEntity();
                rel.setUser(currentUser.getId());
                rel.setPost(post.getId());

                currentUser.getLikesesById().add(rel);
                post.getLikesesById().add(rel);
                likesDao.save(rel);

                getSession().setAttribute("user", currentUser);
            }

            clickEvent.getButton().setCaption(liked == null ? "Unlike" : "Like");
            clickEvent.getButton().setEnabled(true);

            String newtitle = String.format("%s %s - %s - %d like%s",
                    author.getFirstname(),
                    author.getLastname(),
                    new Date(post.getDate().getTime()).toString(),
                    post.getLikesesById().size(),
                    post.getLikesesById().size() > 1 ? "s" : "");
            panel.setCaption(newtitle);
        });

        Button commentButton = new Button("Comment", clickEvent -> {
            Window modal = new Window("Comments");
            modal.setWidth(500.0f, Unit.PIXELS);

            CommentLayout comments = new CommentLayout(usersDao, commentsDao);
            comments.displayComments(currentUser, post.getId());
            modal.setContent(comments);
            modal.setModal(true);
            getUI().getCurrent().addWindow(modal);
        });

        Label date = new Label();

        HorizontalLayout actionLayout = new HorizontalLayout(likeButton, commentButton, date);

        VerticalLayout content = new VerticalLayout(new Label(post.getMessage()), actionLayout);
        content.setWidth(100f, Unit.PERCENTAGE);
        content.setMargin(true);

        panel.setContent(content);
        postsLayout.addComponent(panel, 0);
    }

}
