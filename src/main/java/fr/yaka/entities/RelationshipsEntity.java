package fr.yaka.entities;

import javax.persistence.*;

@Entity
@Table(name = "relationships", schema = "", catalog = "socialdb")
public class RelationshipsEntity extends Model
{
	private int me;
	private int other;
	private UsersEntity usersByMe;
	private UsersEntity usersByOther;

	@Id
	@Column(name = "me")
	public int getMe()
	{
		return me;
	}

	public void setMe(int me)
	{
		this.me = me;
	}

	@Id
	@Column(name = "other")
	public int getOther()
	{
		return other;
	}

	public void setOther(int other)
	{
		this.other = other;
	}

	@Override
	public boolean equals(Object o)
	{
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		RelationshipsEntity that = (RelationshipsEntity) o;

		if (me != that.me) return false;
		if (other != that.other) return false;

		return true;
	}

	@Override
	public int hashCode()
	{
		int result = me;
		result = 31 * result + other;
		return result;
	}

	@ManyToOne
	@JoinColumn(name = "me", referencedColumnName = "id", nullable = false, insertable = false, updatable = false)
	public UsersEntity getUsersByMe()
	{
		return usersByMe;
	}

	public void setUsersByMe(UsersEntity usersByMe)
	{
		this.usersByMe = usersByMe;
	}

	@ManyToOne
	@JoinColumn(name = "other", referencedColumnName = "id", nullable = false, insertable = false, updatable = false)
	public UsersEntity getUsersByOther()
	{
		return usersByOther;
	}

	public void setUsersByOther(UsersEntity usersByOther)
	{
		this.usersByOther = usersByOther;
	}
}
