package fr.yaka.entities;

import fr.yaka.dao.Likes;

import javax.persistence.*;
import java.util.Collection;

@Entity
@Table(name = "users", schema = "", catalog = "socialdb")
public class UsersEntity extends Model
{
	private int id;
	private String lastname;
	private String firstname;
	private String mail;
	private String password;
	private Collection<CommentsEntity> commentsesById;
	private Collection<LikesEntity> likesesById;
	private Collection<PostsEntity> postsesById;
	private Collection<RelationshipsEntity> relationshipsesById;
	private Collection<RelationshipsEntity> relationshipsesById_0;

	@Id
	@Column(name = "id")
	public int getId()
	{
		return id;
	}

	public void setId(int id)
	{
		this.id = id;
	}

	@Basic
	@Column(name = "lastname")
	public String getLastname()
	{
		return lastname;
	}

	public void setLastname(String lastname)
	{
		this.lastname = lastname;
	}

	@Basic
	@Column(name = "firstname")
	public String getFirstname()
	{
		return firstname;
	}

	public void setFirstname(String firstname)
	{
		this.firstname = firstname;
	}

	@Basic
	@Column(name = "mail")
	public String getMail()
	{
		return mail;
	}

	public void setMail(String mail)
	{
		this.mail = mail;
	}

	@Basic
	@Column(name = "password")
	public String getPassword()
	{
		return password;
	}

	public void setPassword(String password)
	{
		this.password = password;
	}

	@Override
	public boolean equals(Object o)
	{
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		UsersEntity that = (UsersEntity) o;

		if (id != that.id) return false;
		if (firstname != null ? !firstname.equals(that.firstname) : that.firstname != null) return false;
		if (lastname != null ? !lastname.equals(that.lastname) : that.lastname != null) return false;
		if (mail != null ? !mail.equals(that.mail) : that.mail != null) return false;
		if (password != null ? !password.equals(that.password) : that.password != null) return false;

		return true;
	}

	@Override
	public int hashCode()
	{
		int result = id;
		result = 31 * result + (lastname != null ? lastname.hashCode() : 0);
		result = 31 * result + (firstname != null ? firstname.hashCode() : 0);
		result = 31 * result + (mail != null ? mail.hashCode() : 0);
		result = 31 * result + (password != null ? password.hashCode() : 0);
		return result;
	}

	@OneToMany(mappedBy = "usersByAuthor")
	public Collection<CommentsEntity> getCommentsesById()
	{
		return commentsesById;
	}

	public void setCommentsesById(Collection<CommentsEntity> commentsesById)
	{
		this.commentsesById = commentsesById;
	}

	@OneToMany(mappedBy = "usersByUser", fetch = FetchType.EAGER)
	public Collection<LikesEntity> getLikesesById()
	{
		return likesesById;
	}

	public void setLikesesById(Collection<LikesEntity> likesesById)
	{
		this.likesesById = likesesById;
	}

	@OneToMany(mappedBy = "usersByAuthor", fetch = FetchType.EAGER)
	public Collection<PostsEntity> getPostsesById()
	{
		return postsesById;
	}

	public void setPostsesById(Collection<PostsEntity> postsesById)
	{
		this.postsesById = postsesById;
	}

	@OneToMany(mappedBy = "usersByMe", fetch = FetchType.EAGER)
	public Collection<RelationshipsEntity> getRelationshipsesById()
	{
		return relationshipsesById;
	}

	public void setRelationshipsesById(Collection<RelationshipsEntity> relationshipsesById)
	{
		this.relationshipsesById = relationshipsesById;
	}

	@OneToMany(mappedBy = "usersByOther", fetch = FetchType.EAGER)
	public Collection<RelationshipsEntity> getRelationshipsesById_0()
	{
		return relationshipsesById_0;
	}

	public void setRelationshipsesById_0(Collection<RelationshipsEntity> relationshipsesById_0)
	{
		this.relationshipsesById_0 = relationshipsesById_0;
	}

	public LikesEntity likesPost(PostsEntity post)
	{
		for (LikesEntity rel : this.getLikesesById())
		{
			if (rel.getUser() == this.getId()  && rel.getPost() == post.getId())
				return rel;
		}
		return null;
	}
}
