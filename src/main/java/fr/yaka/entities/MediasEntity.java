package fr.yaka.entities;

import javax.persistence.*;
import java.util.Collection;

@Entity
@Table(name = "medias", schema = "", catalog = "socialdb")
public class MediasEntity extends Model
{
	private int id;
	private String path;
	private int type;
	private Collection<PostsEntity> postsesById;

	@Id
	@Column(name = "id")
	public int getId()
	{
		return id;
	}

	public void setId(int id)
	{
		this.id = id;
	}

	@Basic
	@Column(name = "path")
	public String getPath()
	{
		return path;
	}

	public void setPath(String path)
	{
		this.path = path;
	}

	@Basic
	@Column(name = "type")
	public int getType()
	{
		return type;
	}

	public void setType(int type)
	{
		this.type = type;
	}

	@Override
	public boolean equals(Object o)
	{
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		MediasEntity that = (MediasEntity) o;

		if (id != that.id) return false;
		if (type != that.type) return false;
		if (path != null ? !path.equals(that.path) : that.path != null) return false;

		return true;
	}

	@Override
	public int hashCode()
	{
		int result = id;
		result = 31 * result + (path != null ? path.hashCode() : 0);
		result = 31 * result + type;
		return result;
	}

	@OneToMany(mappedBy = "mediasByMedia")
	public Collection<PostsEntity> getPostsesById()
	{
		return postsesById;
	}

	public void setPostsesById(Collection<PostsEntity> postsesById)
	{
		this.postsesById = postsesById;
	}
}
