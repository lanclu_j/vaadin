package fr.yaka.entities;

import javax.persistence.*;

@Entity
@Table(name = "likes", schema = "", catalog = "socialdb")
public class LikesEntity extends Model
{
	private int post;
	private int user;
	private PostsEntity postsByPost;
	private UsersEntity usersByUser;

	@Id
	@Column(name = "post")
	public int getPost()
	{
		return post;
	}

	public void setPost(int post)
	{
		this.post = post;
	}

	@Id
	@Column(name = "user")
	public int getUser()
	{
		return user;
	}

	public void setUser(int user)
	{
		this.user = user;
	}

	@Override
	public boolean equals(Object o)
	{
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		LikesEntity that = (LikesEntity) o;

		if (post != that.post) return false;
		if (user != that.user) return false;

		return true;
	}

	@Override
	public int hashCode()
	{
		int result = post;
		result = 31 * result + user;
		return result;
	}

	@ManyToOne
	@JoinColumn(name = "post", referencedColumnName = "id", nullable = false, insertable = false, updatable = false)
	public PostsEntity getPostsByPost()
	{
		return postsByPost;
	}

	public void setPostsByPost(PostsEntity postsByPost)
	{
		this.postsByPost = postsByPost;
	}

	@ManyToOne
	@JoinColumn(name = "user", referencedColumnName = "id", nullable = false, insertable = false, updatable = false)
	public UsersEntity getUsersByUser()
	{
		return usersByUser;
	}

	public void setUsersByUser(UsersEntity usersByUser)
	{
		this.usersByUser = usersByUser;
	}
}
