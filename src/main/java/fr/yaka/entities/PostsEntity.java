package fr.yaka.entities;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Collection;

@Entity
@Table(name = "posts", schema = "", catalog = "socialdb")
public class PostsEntity extends Model
{
	private int id;
	private String message;
	private int author;
	private Timestamp date;
	private Integer media;
	private Collection<CommentsEntity> commentsesById;
	private Collection<LikesEntity> likesesById;
	private UsersEntity usersByAuthor;
	private MediasEntity mediasByMedia;

	@Id
	@Column(name = "id")
	public int getId()
	{
		return id;
	}

	public void setId(int id)
	{
		this.id = id;
	}

	@Basic
	@Column(name = "message")
	public String getMessage()
	{
		return message;
	}

	public void setMessage(String message)
	{
		this.message = message;
	}

	@Basic
	@Column(name = "author")
	public int getAuthor()
	{
		return author;
	}

	public void setAuthor(int author)
	{
		this.author = author;
	}

	@Basic
	@Column(name = "date")
	public Timestamp getDate()
	{
		return date;
	}

	public void setDate(Timestamp date)
	{
		this.date = date;
	}

	@Basic
	@Column(name = "media")
	public Integer getMedia()
	{
		return media;
	}

	public void setMedia(Integer media)
	{
		this.media = media;
	}

	@Override
	public boolean equals(Object o)
	{
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		PostsEntity that = (PostsEntity) o;

		if (author != that.author) return false;
		if (id != that.id) return false;
		if (date != null ? !date.equals(that.date) : that.date != null) return false;
		if (media != null ? !media.equals(that.media) : that.media != null) return false;
		if (message != null ? !message.equals(that.message) : that.message != null) return false;

		return true;
	}

	@Override
	public int hashCode()
	{
		int result = id;
		result = 31 * result + (message != null ? message.hashCode() : 0);
		result = 31 * result + author;
		result = 31 * result + (date != null ? date.hashCode() : 0);
		result = 31 * result + (media != null ? media.hashCode() : 0);
		return result;
	}

	@OneToMany(mappedBy = "postsByPost", fetch = FetchType.EAGER)
	public Collection<CommentsEntity> getCommentsesById()
	{
		return commentsesById;
	}

	public void setCommentsesById(Collection<CommentsEntity> commentsesById)
	{
		this.commentsesById = commentsesById;
	}

	@OneToMany(mappedBy = "postsByPost", fetch = FetchType.EAGER)
	public Collection<LikesEntity> getLikesesById()
	{
		return likesesById;
	}

	public void setLikesesById(Collection<LikesEntity> likesesById)
	{
		this.likesesById = likesesById;
	}

	@ManyToOne
	@JoinColumn(name = "author", referencedColumnName = "id", nullable = false, insertable = false, updatable = false)
	public UsersEntity getUsersByAuthor()
	{
		return usersByAuthor;
	}

	public void setUsersByAuthor(UsersEntity usersByAuthor)
	{
		this.usersByAuthor = usersByAuthor;
	}

	@ManyToOne
	@JoinColumn(name = "media", referencedColumnName = "id", insertable = false, updatable = false)
	public MediasEntity getMediasByMedia()
	{
		return mediasByMedia;
	}

	public void setMediasByMedia(MediasEntity mediasByMedia)
	{
		this.mediasByMedia = mediasByMedia;
	}
}
