package fr.yaka.entities;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@Table(name = "comments", schema = "", catalog = "socialdb")
public class CommentsEntity extends Model
{
	private int id;
	private int post;
	private String message;
	private int author;
	private Timestamp date;
	private UsersEntity usersByAuthor;
	private PostsEntity postsByPost;

	@Id
	@Column(name = "id")
	public int getId()
	{
		return id;
	}

	public void setId(int id)
	{
		this.id = id;
	}

	@Basic
	@Column(name = "post")
	public int getPost()
	{
		return post;
	}

	public void setPost(int post)
	{
		this.post = post;
	}

	@Basic
	@Column(name = "message")
	public String getMessage()
	{
		return message;
	}

	public void setMessage(String message)
	{
		this.message = message;
	}

	@Basic
	@Column(name = "author")
	public int getAuthor()
	{
		return author;
	}

	public void setAuthor(int author)
	{
		this.author = author;
	}

	@Basic
	@Column(name = "date")
	public Timestamp getDate()
	{
		return date;
	}

	public void setDate(Timestamp date)
	{
		this.date = date;
	}

	@Override
	public boolean equals(Object o)
	{
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		CommentsEntity that = (CommentsEntity) o;

		if (author != that.author) return false;
		if (id != that.id) return false;
		if (post != that.post) return false;
		if (date != null ? !date.equals(that.date) : that.date != null) return false;
		if (message != null ? !message.equals(that.message) : that.message != null) return false;

		return true;
	}

	@Override
	public int hashCode()
	{
		int result = id;
		result = 31 * result + post;
		result = 31 * result + (message != null ? message.hashCode() : 0);
		result = 31 * result + author;
		result = 31 * result + (date != null ? date.hashCode() : 0);
		return result;
	}

	@ManyToOne
	@JoinColumn(name = "author", referencedColumnName = "id", nullable = false, insertable = false, updatable = false)
	public UsersEntity getUsersByAuthor()
	{
		return usersByAuthor;
	}

	public void setUsersByAuthor(UsersEntity usersByAuthor)
	{
		this.usersByAuthor = usersByAuthor;
	}

	@ManyToOne
	@JoinColumn(name = "post", referencedColumnName = "id", nullable = false, insertable = false, updatable = false)
	public PostsEntity getPostsByPost()
	{
		return postsByPost;
	}

	public void setPostsByPost(PostsEntity postsByPost)
	{
		this.postsByPost = postsByPost;
	}
}
