package fr.yaka.dao;

import fr.yaka.entities.PostsEntity;

import javax.enterprise.context.Dependent;
import javax.inject.Named;

@Named("Posts")
@Dependent
public class Posts extends DAO<PostsEntity>
{
	public Posts()
	{ super(PostsEntity.class); }
}
