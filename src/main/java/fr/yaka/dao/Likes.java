package fr.yaka.dao;

import fr.yaka.entities.LikesEntity;

import javax.enterprise.context.Dependent;
import javax.inject.Named;

@Named("Likes")
@Dependent
public class Likes extends DAO<LikesEntity>
{
	public Likes()
	{ super(LikesEntity.class); }
}
