package fr.yaka.dao;

import fr.yaka.entities.UsersEntity;

import javax.enterprise.context.Dependent;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.List;

@Named("Users")
@Dependent
public class Users extends DAO<UsersEntity>
{
	public Users()
	{ super(UsersEntity.class); }

	public UsersEntity get(String param)
	{
		EntityManager em = getEntityManager();
		Query q = em.createQuery("FROM UsersEntity WHERE mail = :param");
		List l = q.setParameter("param", param).getResultList();
		return l.isEmpty() ? null : (UsersEntity)l.get(0);
	}

	public List<UsersEntity> search(String param)
	{
		Query q = em.createQuery("FROM UsersEntity WHERE " +
				"firstname LIKE :param" +
				" OR lastname LIKE :param");
		return q.setParameter("param", "%" + param + "%").getResultList();
	}
}
