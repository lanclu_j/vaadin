package fr.yaka.dao;

import fr.yaka.entities.RelationshipsEntity;

import javax.enterprise.context.Dependent;
import javax.inject.Named;

@Named("Relationships")
@Dependent
public class Relationships extends DAO<RelationshipsEntity>
{
	public Relationships()
	{ super(RelationshipsEntity.class); }
}
