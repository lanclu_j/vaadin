package fr.yaka.dao;

import com.vaadin.addon.jpacontainer.provider.MutableLocalEntityProvider;
import fr.yaka.entities.Model;

import javax.annotation.PostConstruct;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.ejb.TransactionManagement;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;
import javax.transaction.Transactional;
import java.io.Serializable;
import java.util.List;

@TransactionManagement
public abstract class DAO<BEAN extends Model> extends MutableLocalEntityProvider<BEAN> implements Serializable
{
	@PersistenceContext(unitName = "vaadinplus", type = PersistenceContextType.EXTENDED)
	protected EntityManager em;

	protected final Class<? extends BEAN> entityClass;
	public DAO(Class<BEAN> entityClass)
	{
		super(entityClass);
		this.entityClass = entityClass;
	}

	@PostConstruct
	public void init() {
		setTransactionsHandledByProvider(false);
		setEntityManager(em);
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	protected void runInTransaction(Runnable operation) {
		super.runInTransaction(operation);
	}

	/**
	 * Find an element in the DAO by it's primary key
	 *
	 * @param id The entity's primary key
	 * @return The element with this primary key
	 */
	public BEAN find(Integer id)
	{
		return getEntityManager().find(entityClass, id);
	}

	/**
	 * Finds all entries of the DAO
	 *
	 * @return The list of all entries in the database
	 */
	public List<BEAN> findAll()
	{
		return getEntityManager().createQuery("FROM " + entityClass.getSimpleName()).getResultList();
	}

	/**
	 * Saves an entity in the database
	 *
	 * @param entity The entity to save
	 * @return Whether the entity was successfully saved or not
	 */
	@Transactional
	public BEAN save(final BEAN entity)
	{
		em.persist(entity);
		return entity;
	}

	@Transactional
	public BEAN update(final BEAN entity)
	{
		getEntityManager().merge(entity);
		return entity;
	}

	@Transactional
	public BEAN delete(final BEAN entity)
	{
		getEntityManager().remove(getEntityManager().merge(entity));
		return entity;
	}
}
