package fr.yaka.dao;

import fr.yaka.entities.CommentsEntity;

import javax.enterprise.context.Dependent;
import javax.inject.Named;

@Named("Comments")
@Dependent
public class Comments extends DAO<CommentsEntity>
{
	public Comments()
	{ super(CommentsEntity.class); }
}
