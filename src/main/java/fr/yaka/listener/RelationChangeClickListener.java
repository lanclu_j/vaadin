package fr.yaka.listener;

import com.vaadin.ui.Button;
import fr.yaka.dao.Relationships;
import fr.yaka.dao.Users;
import fr.yaka.entities.RelationshipsEntity;
import fr.yaka.entities.UsersEntity;

import java.util.Collection;

public class RelationChangeClickListener implements Button.ClickListener {

    private final Users usersDao;
    private final Relationships relationshipsDao;

    public RelationChangeClickListener(Users usersDAO, Relationships relationshipsDAO) {

        this.usersDao = usersDAO;
        this.relationshipsDao = relationshipsDAO;
    }

    @Override
    public void buttonClick(Button.ClickEvent event) {
        UsersEntity current = (UsersEntity) event.getButton().getUI().getSession().getAttribute("user");
        UsersEntity user = (UsersEntity) event.getButton().getData();
        Collection<RelationshipsEntity> relationships = current.getRelationshipsesById();
        RelationshipsEntity rel = new RelationshipsEntity();
        rel.setMe(current.getId());
        rel.setOther(user.getId());
        rel.setUsersByMe(current);
        rel.setUsersByOther(user);
        if (relationships.contains(rel)) {
            for (RelationshipsEntity relation : relationships) {
                if (relation.getMe() == current.getId() && relation.getOther() == user.getId()) {
                    current.getRelationshipsesById().remove(relation);
                    this.relationshipsDao.delete(relation);
                    break;
                }
            }
            event.getButton().getUI().getSession().setAttribute("user", current);
            event.getButton().setCaption("Successfully unfollowed");
            event.getButton().setEnabled(false);
        } else {
            current.getRelationshipsesById().add(rel);
            this.relationshipsDao.save(rel);
            event.getButton().getUI().getSession().setAttribute("user", current);
            event.getButton().setCaption("Successfully followed");
            event.getButton().setEnabled(false);
        }
    }
}
